export const portfolioData = [
  {
    title: 'Foinni',
    text: 'Website for graphic design agency',
    url: 'https://hameleonka.github.io/Foinni/',
    image: '/images/portfolio-foinni.jpeg',
    filter: 'html_css',
  },
  {
    title: 'Project JWood',
    text: 'Photography website',
    url: 'https://hameleonka.github.io/JWood/',
    image: '/images/portfolio-jwood.jpeg',
    filter: 'html_css',
  },
  {
    title: 'Lookshop',
    text: 'Online shop for brand clothes',
    url: 'https://hameleonka.github.io/Lookshop/',
    image: '/images/portfolio-lookshop.jpeg',
    filter: 'js',
  },
  {
    title: 'Velox',
    text: 'Web design agency',
    url: 'https://hameleonka.github.io/Velox/',
    image: '/images/portfolio-velox.jpeg',
    filter: 'js',
  },
  {
    title: 'Glacy',
    text: 'Ice cream shop',
    url: 'https://hameleonka.github.io/133915-gllacy/index.html',
    image: '/images/portfolio-glacy.jpeg',
    filter: 'js',
  },
];

export const expertiseData = [
  {
    date: '2017-2018',
    info: {
      company: 'Mozilla',
      job: 'OUTREACHY program intern',
      description: 'Refactored Firefox Add-ons linter test suite from promise based code to async/await based code',
    },
  },
  {
    date: '2017',
    info: {
      company: 'Code For Social Good',
      job: 'Volunteer front-end web developer',
      description: 'Collaborated on the development of an Angular 2 web application of a global volunteering platform that provides nonprofit organizations with free technical resources',
    },
  },
  {
    date: '2014-2015',
    info: {
      company: 'GasProject Engineering ',
      job: 'Lead engineer',
      description: 'Designed and provided all technical documentation for building  JSC "GAZPROM" security systems (e.g., perimeter and building security, fire alarm and firefighting, closed-circuit television (CCTV) and access control systems)',
    },
  },
  {
    date: '2009-2014',
    info: {
      company: 'RusGas Engineering',
      job: 'Lead engineer',
      description: 'Presented documentation to clients and interacted with multiple contractors. Developed technical decisions and verified technical documentation. Managed a multi-level team of engineers, including prioritizing tasks and measuring performance. Designed and provided all technical documentation for building  security systems (e.g., perimeter and building security, fire alarm and firefighting, closed-circuit television (CCTV) and access control systems) for facilities of oil/gas complex.',
    },
  },
];

export const feedbackData = [
  {
    feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.',
    reporter: { photoUrl: './user.jpg', name: 'John Doe', citeUrl: 'https://www.citeexample.com' },
  },
  {
    feedback: ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.',
    reporter: { photoUrl: './user.jpg', name: 'John Doe', citeUrl: 'https://www.citeexample.com' },
  },
];

export const timelineData = [
  {
    date: 2021,
    title: 'EPAM UpSkill',
    text: 'Online Front end development program',
  },
  {
    date: 2018,
    title: 'CS50',
    text: 'CS50 Introduction to Computer Science course',
  },
  {
    date: 2017,
    title: 'Free Code Camp',
    text: 'Front end development certification',
  },
  {
    date: 2016,
    title: 'HTML academy',
    text: 'HTML, CSS & JavaScript course',
  },
  {
    date: 2007,
    title: 'Ufa State Aviation Technical University',
    text: 'Master\'s Degree in Engineering, Information and measuring equipment and technology',
  },

];

export const skillsData = [
  {
    name: 'HTML',
    range: 100,
  },
  {
    name: 'CSS',
    range: 90,
  },
  {
    name: 'JavaScript',
    range: 80,
  },
  {
    name: 'ReactJS',
    range: 70,
  },
];
