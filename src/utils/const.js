/* eslint-disable import/prefer-default-export */
export const requestState = {
  PENDING: 0,
  FULFILLED: 1,
  FAILED: 2,
  INITIAL: 3,
  STORED: 4,
};
